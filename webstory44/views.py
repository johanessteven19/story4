from django.shortcuts import render
from django.http import HttpResponse
from .forms import DataForm
from .models import Data

# Create your views here.
def home(request):
    return render(request, 'webstory44/index.html')

def profile(request):
    return render(request, 'webstory44/profile.html')

def message(request):
    return render(request, 'webstory44/message.html')

def contacts(request):
    return render(request, 'webstory44/contacts.html')

def add(request):
    if request.method == 'POST':
        form = DataForm(request.POST)
        if form.is_valid():
            data_item = form.save(commit=False)
            data_item.save()
            return render(request, 'webstory44/add.html')
    else:
        form = DataForm()
        return render(request, 'webstory44/add.html', {'form' : form})


def see(request):
    listname = Data.objects.all()
    context = {'listname': listname}
    return render(request, 'webstory44/see.html', context)





