from django.db import models
from django.conf import settings

class Year(models.Model):
    year = models.CharField(primary_key=True, max_length=100)
    def __str__(self):
        return '%s' % self.year

class Data(models.Model):
    name = models.CharField(primary_key=True, max_length=100)
    hobby = models.CharField(max_length=100)
    fav = models.CharField(max_length=100)
    classyear = models.ForeignKey(Year, on_delete=models.CASCADE)
    
    def __str__(self):
        return '%s' % self.name